# Overview

This repository is a copy from https://github.com/learn-chef/lcr-policyfiles-getstarted.git. We are using it to learn about Policyfiles. We created a wrapper cookbook called base that inherit the hardening cookbook.

# Repository Directories

This repository contains several directories, and each directory contains a README file that describes what it is for in greater detail, and how to use it for managing your systems with Chef.

- `cookbooks/` - Cookbooks you download or create.
- `data_bags/` - Store data bags and items in .json in the repository.
- `roles/` - Store roles in .rb or .json in the repository.
- `environments/` - Store environments in .rb or .json in the repository.

# Details about base cookbook creation

1. The new cookbook was generated

```bash
chef generate cookbook cookbook/base
```

2. Include the hardening recipe to base cookbook

On file cookbooks/base/recipes/default.rb, add the content bellow

```bash
include_recipe 'hardening::default'
```

3. Edit the metabase.rb from base cookbook

```bash
depends 'hardening'
```

4. Edit the Policyfile.rb from base cookbook

```bash
cookbook 'hardening', path: '../hardening'
```

# How to run

1. Install the Policyfile

```bash
chef install cookbooks/base/Policyfile.rb
```

2. Check the information generated

```bash
chef describe-cookbook cookbooks/base
```

3. Test with kitchen

```bash
cd cookbooks/base
kitchen converge centos
kitchen login centos
```

4. Update the Policyfile lock.json (If you need to change something)

```bash
chef update cookbooks/base/Policyfile.rb
```

# Clean your environment

```bash
cd cookbooks/base
kitchen destroy centos
```

# Next Steps

Read the README file in each of the subdirectories for more information about what goes in those directories.
